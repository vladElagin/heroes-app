import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { Root, Container } from './components/atoms';
import { Header } from './components/molecules';
import { HeroesList, NotFound } from './components/organisms';
import theme from './themes/main';
import GlobalStyles from './themes/global';
import './assets/fontello.css';
import store from './store';

const App = () => {
  return (
    <>
      <GlobalStyles />
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <Router>
            <Root>
              <Switch>
                <Route
                  path="/"
                  exact
                  render={() => (
                    <Container>
                      <Header />
                      <HeroesList />
                    </Container>
                  )}
                />
                <Route path="/*" component={NotFound} />
              </Switch>
            </Root>
          </Router>
        </Provider>
      </ThemeProvider>
    </>
  );
};

export default App;
