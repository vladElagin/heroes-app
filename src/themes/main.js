const theme = {
  colors: {
    rootBackground: '#f5f6fb',
    heroItemBackground: '#fff',
    success: '#60cc90',
    primary: '#6198f3',
    textBlack: '#222',
    textDark: '#8b919d',
    danger: '#d93a58',
  },
  fontWeights: {
    regular: 400,
    semibold: 600,
    bold: 900,
  },
  fontSizes: {
    lg: '21px',
  },
};

export default theme;
