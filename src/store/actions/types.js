import callApi from '../../utils/callApi';

export const SET_HERO_TYPES = 'SET_HERO_TYPES';

const setHeroTypes = (types) => ({ type: SET_HERO_TYPES, payload: types });

export const loadHeroTypes = () => {
  return (dispatch) => {
    callApi('GET', '/types').then((types) => {
      dispatch(setHeroTypes(types));
    });
  };
};
