export {
  LOAD_HEROES_SUCCESS,
  LOAD_HEROES_FAILURE,
  SET_HEROES_LOADING,
  INCREASE_HEROES_PAGE,
  SET_LOADED_ALL,
  RESET_HEROES_STATE,
  SEARCH,
  searchHero,
  loadHeroes,
  increaseHeroesPage,
  deleteHero,
  createHero,
  editHero
} from './heroes';

export { SET_HERO_TYPES, loadHeroTypes } from './types';
