import callApi from '../../utils/callApi';

export const LOAD_HEROES_SUCCESS = 'LOAD_HEROES_SUCCESS';
export const LOAD_HEROES_FAILURE = 'LOAD_HEROES_FAILURE';
export const SET_HEROES_LOADING = 'SET_HEROES_LOADING';
export const INCREASE_HEROES_PAGE = 'INCREASE_HEROES_PAGE';
export const SET_LOADED_ALL = 'SET_LOADED_ALL';
export const RESET_HEROES_STATE = 'RESET_HEROES_STATE';
export const SEARCH = 'SEARCH';

export const setHeroesLoading = (to = true) => ({
  type: SET_HEROES_LOADING,
  payload: to,
});

export const increaseHeroesPage = () => ({
  type: INCREASE_HEROES_PAGE,
});

export const resetHeroesState = () => ({ type: RESET_HEROES_STATE });

const loadHeroesSuccess = (heroes, reset) => ({
  type: LOAD_HEROES_SUCCESS,
  payload: { heroes, reset },
});

const loadHeroFailure = () => ({
  type: LOAD_HEROES_FAILURE,
});

const setLoadedAll = () => ({ type: SET_LOADED_ALL });

export const loadHeroes = (reset = false) => {
  return (dispatch, getState) => {
    const { page, loadedAll } = getState().heroes;
    if (loadedAll) {
      return;
    }

    dispatch(setHeroesLoading(true));

    const HEROES_BY_PAGE = 10;
    const url = `/heroes?first=${HEROES_BY_PAGE}&skip=${
      (page - 1) * HEROES_BY_PAGE
    }`;

    callApi('GET', url)
      .then((data) => {
        dispatch(loadHeroesSuccess(data, reset));

        dispatch(setHeroesLoading(false));

        if (data.length % 10 !== 0 || !data.length) {
          dispatch(setLoadedAll());
        }
      })
      .catch(() => {
        dispatch(loadHeroFailure());
      });
  };
};

export const deleteHero = (id) => {
  return (dispatch) => {
    dispatch(setHeroesLoading(true));
    dispatch(resetHeroesState());

    callApi('DELETE', `/heroes/${id}`).then(() => {
      dispatch(loadHeroes(true));
    });
  };
};

export const createHero = (data) => {
  return (dispatch) => {
    dispatch(setHeroesLoading(true));
    dispatch(resetHeroesState());

    callApi('POST', '/heroes', data).then(() => {
      dispatch(loadHeroes(true));
    });
  };
};

export const editHero = (id, data) => {
  return (dispatch) => {
    dispatch(setHeroesLoading(true));
    dispatch(resetHeroesState());

    callApi('PUT', `/heroes/${id}`, data).then(() => {
      dispatch(loadHeroes(true));
    });
  };
};

export const searchHero = (payload) => {
  return { type: SEARCH, payload };
};
