import { combineReducers } from 'redux';

import heroesReducer from './heroes';
import heroTypesReducer from './types';

const rootReducer = combineReducers({
  heroes: heroesReducer,
  heroTypes: heroTypesReducer,
});

export default rootReducer;
