import { SET_HERO_TYPES } from '../actions';

const initialState = {
  heroTypes: [],
};

const heroTypesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_HERO_TYPES:
      return {
        ...state,
        heroTypes: payload,
      };

    default:
      return state;
  }
};

export default heroTypesReducer;
