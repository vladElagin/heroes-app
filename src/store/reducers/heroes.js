import {
  LOAD_HEROES_SUCCESS,
  SET_HEROES_LOADING,
  INCREASE_HEROES_PAGE,
  SET_LOADED_ALL,
  RESET_HEROES_STATE,
  SEARCH
} from '../actions';
import { LOAD_HEROES_FAILURE } from '../actions/heroes';

const initialState = {
  page: 1, // how many heros were loaded
  isLoading: false, // display spinner over heroes list
  loadedAll: false, // loaded all heroes, hide 'load more'
  heroes: [],
  error: false,
  searchValue: ''
};

const heroesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_HEROES_LOADING:
      return {
        ...state,
        isLoading: payload,
      };

    case LOAD_HEROES_SUCCESS:
      return {
        ...state,
        heroes: payload.reset
          ? [...payload.heroes]
          : [...state.heroes, ...payload.heroes],
        error: null,
      };

    case LOAD_HEROES_FAILURE:
      return {
        heroes: [],
        error: true,
      };

    case INCREASE_HEROES_PAGE:
      return {
        ...state,
        page: state.page + 1,
      };

    case SET_LOADED_ALL:
      return {
        ...state,
        loadedAll: true,
      };

    case RESET_HEROES_STATE:
      return {
        ...state,
        heroes: [],
        loadedAll: false,
        page: 1,
      };
    case SEARCH:
        return {
          ...state,
          searchValue: payload,
        };

    default:
      return state;
  }
};

export default heroesReducer;
