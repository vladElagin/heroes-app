import axios from 'axios';

const baseUrl = 'http://localhost:4000';

/**
 * Custom HTTP calls handler.
 * Its main purpose is to create concurrent promises:
 * 1. Actual http call
 * 2. one that automatically resolves after timeout
 *
 * This allows to avoid flickering on preloader and results in more smooth UX.
 * Also here we can change root url of calls to external API.
 */
const callApi = (method, url, data = null) => {
  return axios({
    method,
    url: `${baseUrl}${url}`,
    data,
  }).then((res) => Promise.resolve(res.data));
};

export default callApi;
