// eslint-disable-next-line import/prefer-default-export
export const heroes = [
  {
    full_name: 'Batman',
    description:
      'Sed nec venenatis felis. Aenean efficitur et massa auctor auctor.',
    id: 'ck9cnpxl800c70730smrszztb',
    type: { id: 'ck9cnpxj900ap0730co940xks', name: 'Human' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Wrestler',
    description:
      'Sed nec venenatis felis. Aenean efficitur et massa auctor auctor.',
    id: 'ck9cnpxl800c807301mel1lb1',
    type: { id: 'ck9cnpxj900ap0730co940xks', name: 'Human' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Donald Trump',
    description:
      'Sed nec venenatis felis. Aenean efficitur et massa auctor auctor.',
    id: 'ck9cnpxl800c90730wdxhp172',
    type: { id: 'ck9cnpxj900ap0730co940xks', name: 'Human' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Harley Quinn',
    description:
      'Sed nec venenatis felis. Aenean efficitur et massa auctor auctor.',
    id: 'ck9cnpxl800ca07303wqzr869',
    type: { id: 'ck9cnpxj900ap0730co940xks', name: 'Human' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Albert Einstain',
    description:
      'Sed nec venenatis felis. Aenean efficitur et massa auctor auctor.',
    id: 'ck9cnpxl800cb07303fcqh5rx',
    type: { id: 'ck9cnpxj900ap0730co940xks', name: 'Human' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Ozzy',
    description:
      'Sed nec venenatis felis. Aenean efficitur et massa auctor auctor.',
    id: 'ck9cnpxlb00cc0730hpq38gzk',
    type: { id: 'ck9cnpxj900ap0730co940xks', name: 'Human' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Sluggard',
    description:
      'Quisque diam sapien, euismod sed ornare feugiat, vulputate nec tellus.',
    id: 'ck9cnpxlb00cd0730d198yzqq',
    type: { id: 'ck9cnpxjm00au0730meexu15c', name: 'Animal' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Cool Sheep',
    description:
      'Quisque diam sapien, euismod sed ornare feugiat, vulputate nec tellus.',
    id: 'ck9cnpxlb00ce0730d91auy4d',
    type: { id: 'ck9cnpxjm00au0730meexu15c', name: 'Animal' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'The Cactus',
    description:
      'Quisque diam sapien, euismod sed ornare feugiat, vulputate nec tellus.',
    id: 'ck9cnpxlb00cf0730vafbwkmw',
    type: { id: 'ck9cnpxkc00b40730rdmp0p8y', name: 'Plant' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'The Avocado',
    description:
      'Quisque diam sapien, euismod sed ornare feugiat, vulputate nec tellus.',
    id: 'ck9cnpxlg00cg0730eme3opdi',
    type: { id: 'ck9cnpxkc00b40730rdmp0p8y', name: 'Plant' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'Shelba',
    description:
      'Pellentesque efficitur, nisl et pulvinar iaculis, lacus eros faucibus leo, quis vestibulum quam velit ac sapien.',
    id: 'ck9cnpxli00ch0730zh4ho2fx',
    type: { id: 'ck9cnpxk000az07308e3xjv2r', name: 'Other' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
  {
    full_name: 'UFO',
    description:
      'Pellentesque efficitur, nisl et pulvinar iaculis, lacus eros faucibus leo, quis vestibulum quam velit ac sapien.',
    id: 'ck9cnpxli00ci0730czoqo2ep',
    type: { id: 'ck9cnpxk000az07308e3xjv2r', name: 'Other' },
    avatar_url:
      'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hamburger.png',
  },
];

export const types = [
  { id: 'ck9fdkwhs00ry0751nexnates', name: 'Human' },
  { id: 'ck9fdkwia00s30751lepphrsi', name: 'Animal' },
  { id: 'ck9fdkwit00s807515cn7lghx', name: 'Other' },
  { id: 'ck9fdkwja00sd0751y5o4yomv', name: 'Plant' },
];
