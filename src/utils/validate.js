const urlRegex = /(https?:\/\/.*\.(?:png|jpg|jpeg))/i;

const validate = ({ avatar_url, full_name, type }) => {
  const errors = {};

  if (!avatar_url) {
    errors.avatar_url = 'Avatar url is required.';
  } else if (!urlRegex.test(avatar_url)) {
    errors.avatar_url =
      'Avatar url should be a valid image link! Example: http://yourpage.com/image.png `';
  }

  if (!full_name) {
    errors.full_name = 'Full name is required.';
  }

  if (!type) {
    errors.type = 'Type is required.';
  }

  return errors;
};

export default validate;
