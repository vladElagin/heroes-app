import React from 'react';
import {
  render,
  waitForElement,
  fireEvent,
  waitForElementToBeRemoved,
} from '@testing-library/react';

import moxios from 'moxios';

import App from './App';
import { heroes, types } from './utils/testMocks';

jest.setTimeout(10000);

describe('Heroes CRUD app', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  test("renders 'new hero' link", () => {
    const { getByText } = render(<App />);
    const linkElement = getByText(/Add hero/i);
    expect(linkElement).toBeInTheDocument();
  });

  test('Renders heroes and  load more button', (done) => {
    const {
      getByText,
      findAllByTestId,
      findByTestId,
      getByTestId,
      queryByText,
    } = render(<App />);
    moxios.wait(async () => {
      // app initially makes 2 http calls - heroes and their types. check that they were made
      expect(moxios.requests.count()).toBe(2);

      // mock response to heroes request
      await moxios.requests.at(0).respondWith({
        status: 200,
        response: heroes.slice(0, 10),
      });

      // mock response to hero types request
      await moxios.requests.at(1).respondWith({ status: 200, response: types });

      // wait until heroes are rendered
      expect(await findAllByTestId('hero-wrapper')).toHaveLength(10);

      // check that load more button was rendered
      await waitForElement(() =>
        expect(expect(getByText('Load More')).toBeInTheDocument())
      );

      // click load more button
      fireEvent.click(getByText('Load More'));

      // wait until spinner appears
      await waitForElement(async () =>
        expect(await findByTestId('spinner')).toBeInTheDocument()
      );

      // third http call should be made to load second page of heroes
      expect(moxios.requests.count()).toBe(3);

      // respond with heroes left
      await moxios.requests.at(2).respondWith({
        status: 200,
        response: heroes.slice(10, heroes.length),
      });

      // wait for spinner to be removed
      // await waitForElementToBeRemoved(() => getByTestId('spinner'));

      // check that all heroes were rendered
      expect(await findAllByTestId('hero-wrapper')).toHaveLength(heroes.length);

      // check that load more button disappeared
      expect(await queryByText('Load More')).not.toBeInTheDocument();

      // check that user is notified that there are no heroes left to load
      expect(
        await queryByText('We have loaded all heroes for you')
      ).toBeInTheDocument();

      done();
    });
  });
});
