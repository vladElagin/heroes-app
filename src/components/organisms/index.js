import HeroesList from './HeroesList';
import DetailsModal from './DetailsModal';
import CreateHeroModal from './CreateModal';
import NotFound from './NotFound';

export { HeroesList, DetailsModal, CreateHeroModal, NotFound };
