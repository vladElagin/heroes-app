import React from 'react';
import pt from 'prop-types';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';

import {
  Modal,
  Avatar,
  Text,
  EmphasisedText,
  Button,
  ModalContent,
} from '../atoms';
import { deleteHero } from '../../store/actions';

const DetailsModalContent = styled(ModalContent)`
  padding-top: 20px;
  align-items: center;
`;

const DetailsModalAvatar = styled(Avatar)`
  width: 120px;
  height: 120px;
  margin-bottom: 15px;
`;

const DetailsModalType = styled(Text)`
  margin-top: 10px;
  margin-bottom: 30px;
`;

const DetailsModalDescription = styled.p`
  margin: 0;
  padding: 0 20px;
  line-height: 30px;
  word-break: break-word;

  @media (max-width: 425px) {
    padding: 0 10px;
  }
`;

const DetailsModalDeleteButton = styled(Button).attrs(({ onClick }) => ({
  onClick,
}))`
  background: transparent;
  color: ${({ theme }) => theme.colors.danger};
  flex-shrink: 0;
`;

const DetailsModalEditButton = styled(Button).attrs(({ onClick }) => ({
  onClick,
}))`
  background: transparent;
  flex-shrink: 0;
  color: ${({ theme }) => theme.colors.success};
  margin-top: 15px;

  @media (max-width: 425px) {
    margin-top: auto;
  }
`;

const HeroName = styled(EmphasisedText)`
  width: 100%;
  text-align: center;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  flex-shrink: 0;
`;

const DetailsModal = ({ id, onClose, onEditClick }) => {
  const {
    avatar_url,
    full_name,
    type,
    description,
  } = useSelector(({ heroes }) => heroes.heroes.find((h) => h.id === id));
  const dispatch = useDispatch();

  const onDeleteClick = () => {
    onClose();
    dispatch(deleteHero(id));
  };

  return (
    <Modal onClose={onClose}>
      <DetailsModalContent>
        <DetailsModalAvatar src={avatar_url} />
        <HeroName>{full_name}</HeroName>
        <DetailsModalType>{type.name}</DetailsModalType>
        <DetailsModalDescription>{description}</DetailsModalDescription>
        <DetailsModalEditButton onClick={onEditClick}>
          <i className="icon-edit" />
          &nbsp;Edit hero
        </DetailsModalEditButton>
        <DetailsModalDeleteButton onClick={onDeleteClick}>
          <i className="icon-trash" />
          &nbsp;Delete hero
        </DetailsModalDeleteButton>
      </DetailsModalContent>
    </Modal>
  );
};

DetailsModal.propTypes = {
  id: pt.string.isRequired,
  onClose: pt.func.isRequired,
  onEditClick: pt.func.isRequired,
};

export default DetailsModal;
