import React, { useState, useEffect, useCallback } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';

import { Spinner, Notification, Text } from '../atoms';
import { Hero, LoadMore } from '../molecules';
import { loadHeroes, loadHeroTypes } from '../../store/actions';

const HeroesWrapper = styled.main`
  min-height: 250px;
  position: relative;
  padding-bottom: 30px;

  @media (min-width: 425px) {
    padding-bottom: 15px;
  }
`;

const NoHeroesMessage = styled(Notification)`
  text-align: center;
  padding-top: 25px;
`;

const Columns = styled.div`
  display: flex;

  @media (max-width: 425px) {
    display: none;
  }
`;

const ColumnName = styled(Text)`
  color: ${({ theme }) => theme.colors.textDark};

  :first-of-type {
    flex: calc(25% + 80px) 1 1;
  }

  :nth-of-type(2) {
    flex: 25% 1 1;
  }

  :last-of-type {
    flex: calc(40% + 5px) 0 0;
  }
`;

const HeroesList = () => {
  const dispatch = useDispatch();
  const {
    heroes: heroesList,
    isLoading,
    page,
    error,
    searchValue,
  } = useSelector(({ heroes }) => heroes);

  useEffect(() => {
    dispatch(loadHeroes());
  }, [dispatch, page]);

  useEffect(() => {
    dispatch(loadHeroTypes());
  }, [dispatch]);

  const [filteredHeroes, setfilteredHeroes] = useState([]);

  const filterHeroes = useCallback(() => {
    if (searchValue) {
      const filteredHeroes = heroesList.filter((hero) => {
        const lcHeroName = hero?.full_name?.toLowerCase();
        const lcSearchValue = searchValue?.toLowerCase();
        const doesHeroIncludeSearchValue = lcHeroName?.includes(lcSearchValue);
        return doesHeroIncludeSearchValue;
      });
      setfilteredHeroes(filteredHeroes);
    } else {
      setfilteredHeroes(heroesList);
    }
  }, [searchValue, heroesList]);

  useEffect(() => {
    filterHeroes();
  }, [heroesList, filterHeroes]);

  return (
    <HeroesWrapper>
      {isLoading ? <Spinner /> : null}

      {!isLoading && !heroesList.length && !error ? (
        <NoHeroesMessage>
          There are no heroes left in this world...
        </NoHeroesMessage>
      ) : null}

      {!isLoading && error ? (
        <NoHeroesMessage>Could not get data from database</NoHeroesMessage>
      ) : null}

      {heroesList.length && !error ? (
        filteredHeroes.length > 0 ? (
          <>
            <Columns>
              <ColumnName>Heros</ColumnName>
              <ColumnName>Type</ColumnName>
              <ColumnName>Description</ColumnName>
            </Columns>

            {filteredHeroes.map((h) => (
              <Hero key={h.id} {...h} />
            ))}
          </>
        ) : (
          <h2>Sorry, we could not find any matching hero</h2>
        )
      ) : null}

      {}

      {heroesList.length ? <LoadMore /> : null}
    </HeroesWrapper>
  );
};

export default HeroesList;
