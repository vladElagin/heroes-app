import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import pt from 'prop-types';
import { useFormik } from 'formik';

import AvocadoImage from '../../assets/avocado.png';
import { createHero, editHero } from '../../store/actions';
import {
  Modal,
  ModalContent,
  Avatar,
  InputGroup,
  Input,
  SuccessButton,
  EmphasisedText,
  Textarea,
  Select,
} from '../atoms';
import validate from '../../utils/validate';

const CreateHeroModalContent = styled(ModalContent)`
  align-items: flex-start;
  padding: 20px 20px 0;
`;

const ModalHeading = styled(EmphasisedText)`
  font-size: 18px;
  margin-bottom: 35px;
`;

const Form = styled.form`
  width: 100%;
`;

const CreateHeroModalAvatar = styled(Avatar).attrs(({ src }) => ({
  src: src || AvocadoImage,
}))`
  width: 120px;
  height: 120px;
  margin-bottom: 15px;
`;

const SelectArrow = styled.i.attrs({ className: 'icon-down-open' })`
  position: absolute;
  top: 40px;
  right: 20px;
  color: ${({ theme }) => theme.colors.textDark};
  pointer-events: none;
`;

const ErrorMessage = styled.span`
  color: ${({ theme }) => theme.colors.danger};
  margin-top: 5px;
  font-size: 12px;
  padding-left: 10px;
  display: block;
`;

const SaveButton = styled(SuccessButton).attrs({ type: 'submit' })`
  width: 100%;
`;

const CreateHeroModal = ({ onClose, fieldValues, mode, heroId }) => {
  const dispatch = useDispatch();
  const { options } = useSelector(({ heroTypes }) => ({
    options: heroTypes.heroTypes,
  }));

  const { handleSubmit, getFieldProps, errors } = useFormik({
    initialValues: fieldValues,
    onSubmit: (values) => {
      onClose();
      if (mode === 'create') {
        dispatch(createHero(values));
      }
      if (mode === 'edit') {
        dispatch(editHero(heroId, values));
      }
    },
    validateOnBlur: false,
    validateOnChange: false,
    validate,
  });

  return (
    <Modal onClose={onClose}>
      <CreateHeroModalContent>
        <ModalHeading>
          {mode === 'create' ? 'Add' : 'Edit'}
          &nbsp;hero
        </ModalHeading>
        <CreateHeroModalAvatar src={fieldValues.avatar_url} />
        <Form onSubmit={handleSubmit}>
          <InputGroup label="Avatar URL">
            <Input
              id={'Avatar URL'.toLowerCase().replace(' ', '')}
              {...getFieldProps('avatar_url')}
              name="avatar_url"
              error={errors.avatar_url}
            />
            {errors.avatar_url && (
              <ErrorMessage>{errors.avatar_url}</ErrorMessage>
            )}
          </InputGroup>
          <InputGroup label="Full name">
            <Input
              id={'Full name'.toLowerCase().replace(' ', '')}
              {...getFieldProps('full_name')}
              name="full_name"
              error={errors.full_name}
            />
            {errors.full_name && (
              <ErrorMessage>{errors.full_name}</ErrorMessage>
            )}
          </InputGroup>
          <InputGroup label="Type">
            <Select
              id={'Type'.toLowerCase().replace(' ', '')}
              {...getFieldProps('type')}
              name="type"
              error={errors.type}
            >
              <>
                <option disabled />
                {options.map((opt) => (
                  <option key={opt.id} value={opt.id}>
                    {opt.name}
                  </option>
                ))}
              </>
            </Select>
            {errors.type && <ErrorMessage>{errors.type}</ErrorMessage>}
            <SelectArrow />
          </InputGroup>
          <InputGroup label="Description">
            <Textarea
              id={'Description'.toLowerCase().replace(' ', '')}
              rows="3"
              {...getFieldProps('description')}
              name="description"
              error={errors.description}
            />
          </InputGroup>
          <SaveButton>Save</SaveButton>
        </Form>
      </CreateHeroModalContent>
    </Modal>
  );
};

CreateHeroModal.propTypes = {
  onClose: pt.func.isRequired,
  mode: pt.oneOf(['create', 'edit']).isRequired,
  fieldValues: pt.shape({
    id: pt.string,
    avatar_url: pt.string,
    full_name: pt.string,
    type: pt.string,
    description: pt.string,
  }),
  heroId: pt.string,
};

CreateHeroModal.defaultProps = {
  fieldValues: {
    avatar_url: '',
    full_name: '',
    type: '',
    description: '',
  },
  heroId: null,
};

export default CreateHeroModal;
