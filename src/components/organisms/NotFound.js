import React from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { EmphasisedText, Button } from '../atoms';

const NotFoundWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column nowrap;
  height: 100vh;
  padding: 0 15px;
`;

const Message = styled.span`
  color: ${({ theme }) => theme.colors.primary};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  font-size: 140px;
  margin-bottom: 30px;

  @media (max-width: 768px) {
    font-size: 80px;
  }
`;

const SubMessage = styled(EmphasisedText)`
  font-size: 30px;
  margin-bottom: 40px;
  color: ${({ theme }) => theme.colors.textBlack};

  @media (max-width: 768px) {
    font-size: 24px;
  }
`;

const RedirectButton = styled(Button)`
  background-color: transparent;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.primary};
  font-weight: ${({ theme }) => theme.fontWeights.semibold};
`;

const NotFound = () => {
  const history = useHistory();

  const onClick = () => {
    history.push('/');
  };

  return (
    <NotFoundWrapper>
      <Message>OOPS!</Message>
      <SubMessage>
        We can&apos;t find the page you&apos;re looking for.
      </SubMessage>
      <RedirectButton onClick={onClick}>Visit homepage</RedirectButton>
    </NotFoundWrapper>
  );
};

export default NotFound;
