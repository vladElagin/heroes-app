import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { PrimaryButton } from '../atoms';
import { increaseHeroesPage } from '../../store/actions';

const LoadMoreWrapper = styled.div`
  padding-top: 30px;
  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-width: 425px) {
    padding-top: 15px;
    button {
      width: 100%;
    }
  }
`;

const LoadMoreButton = styled(PrimaryButton)`
  padding: 0 45px;
  opacity: ${({ disabled }) => (disabled ? '0.4' : '1')};
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
`;

const LoadMore = () => {
  const dispatch = useDispatch();
  const disabled = useSelector(({ heroes }) => heroes.loadedAll);

  const onClick = () => {
    dispatch(increaseHeroesPage());
  };

  return (
    <LoadMoreWrapper>
      <LoadMoreButton onClick={onClick} disabled={disabled}>
        {disabled ? 'We have loaded all heroes for you' : 'Load More'}
      </LoadMoreButton>
    </LoadMoreWrapper>
  );
};

export default LoadMore;
