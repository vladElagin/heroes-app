import React, { useState } from 'react';
import pt from 'prop-types';
import styled from 'styled-components';

import { Avatar, Text, EmphasisedText, TruncatedText } from '../atoms';
import { DetailsModal, CreateHeroModal } from '../organisms';

const HeroWrapper = styled.div.attrs(({ onClick }) => ({
  onClick,
  'data-testid': 'hero-wrapper',
}))`
  background-color: ${({ theme }) => theme.colors.heroItemBackground};
  border-radius: 10px;
  cursor: pointer;
  margin-top: 10px;
  padding: 20px 5px;

  display: flex;
  align-items: center;

  @media (max-width: 425px) {
    flex-wrap: wrap;
  }
`;

const HeroAvatar = styled(Avatar)`
  min-width: 40px;
  width: 40px;
  height: 40px;
  margin-left: 5px;
  margin-right: 35px;

  @media (max-width: 425px) {
    margin-left: 20px;
    margin-right: 30px;
  }
`;

const HeroData = styled.div`
  flex: 25% 1 1;
  overflow: hidden;
  padding-right: 10px;
  text-overflow: ellipsis;

  span:first-of-type {
    width: calc(100%);
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  span:last-of-type {
    display: none;
  }

  @media (max-width: 425px) {
    flex: calc(100% - 90px) 1 1;

    span:last-of-type {
      display: block;
    }
  }
`;

const HeroType = styled.div`
  flex: 25% 1 1;

  @media (max-width: 425px) {
    display: none;
  }
`;

const HeroDescription = styled.div`
  flex: 40% 0 0;
  width: auto;
  overflow: hidden;
  text-overflow: ellipsis;

  @media (max-width: 425px) {
    flex: 100% 1 1;
    margin-top: 15px;
    padding: 0 15px;
  }
`;

const Hero = ({ id, full_name, description, type, avatar_url }) => {
  const [showDetails, setShowDetails] = useState(false);
  const [showEdit, setShowEdit] = useState(false);

  const onShowDetails = () => {
    setShowDetails(true);
  };

  const onCloseDetails = () => {
    setShowDetails(false);
  };

  const onShowEdit = () => {
    if (showDetails) {
      setShowDetails(false);
    }
    setShowEdit(true);
  };

  const onCloseEdit = () => {
    setShowEdit(false);
  };

  return (
    <>
      <HeroWrapper onClick={onShowDetails}>
        <HeroAvatar src={avatar_url} />
        <HeroData>
          <EmphasisedText>{full_name}</EmphasisedText>
          <Text>{type.name}</Text>
        </HeroData>
        <HeroType>{type.name}</HeroType>
        <HeroDescription>
          <TruncatedText>{description}</TruncatedText>
        </HeroDescription>
      </HeroWrapper>
      {showDetails && (
        <DetailsModal
          id={id}
          onClose={onCloseDetails}
          onEditClick={onShowEdit}
        />
      )}

      {showEdit && !showDetails && (
        <CreateHeroModal
          onClose={onCloseEdit}
          mode="edit"
          heroId={id}
          fieldValues={{
            full_name,
            description,
            type: type.id,
            avatar_url,
          }}
        />
      )}
    </>
  );
};

Hero.propTypes = {
  id: pt.string.isRequired,
  full_name: pt.string.isRequired,
  description: pt.string.isRequired,
  type: pt.shape({ id: pt.string, name: pt.string }).isRequired,
  avatar_url: pt.string.isRequired,
};

export default Hero;
