import React, { useState } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';

import { SuccessButton, Input } from '../atoms';
import { CreateHeroModal } from '../organisms';
import { searchHero } from '../../store/actions/heroes';

const StyledHeader = styled.header`
  padding: 30px 0;
  display: flex;
  justify-content: space-between;
  button{
    width: 150px;
  }
  input{
    width: 150px;
  }


  @media (max-width: 425px) {
    padding: 15px 0;
    button {
      width: 49%;
    }
    input{
      width: 49%;
    }
  }
`;

const Header = () => {
  const dispatch = useDispatch();
  
  const [showNewHeroModal, setShowNewHeroModal] = useState(false);
  const disableNew = useSelector(({ heroes }) => heroes.error);

  const handleInputChange = e =>dispatch(searchHero(e.target.value));

  return (
    <>
      <StyledHeader>
        <SuccessButton
          onClick={() => {
            if (!disableNew) {
              setShowNewHeroModal(true);
            }
          }}
          disabled={disableNew}
        >
          <i className="icon-plus" />
          &nbsp; Add hero
        </SuccessButton>
        <Input placeholder="search hero" disabled={disableNew} onChange={handleInputChange}/>
      </StyledHeader>
      {showNewHeroModal && (
        <CreateHeroModal
          onClose={() => setShowNewHeroModal(false)}
          mode="create"
        />
      )}
    </>
  );
};

export default Header;
