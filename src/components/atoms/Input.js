import React from 'react';
import styled from 'styled-components';
import pt from 'prop-types';

const InputGroupWrapper = styled.div`
  width: 100%;
  margin-bottom: 25px;
  position: relative;
`;

const Label = styled.label`
  color: ${({ theme }) => theme.colors.textDark};
  font-size: 14px;
  margin-bottom: 10px;
  display: block;
  font-weight: ${({ theme }) => theme.fontWeights.semibold};
`;

// mixin for both input and textarea
const inputStyling = ({ theme, error }) => `
  background-color: ${theme.colors.heroItemBackground};
  border-radius: 10px;
  border-width: 1px;
  border-style: solid;
  border-color: ${
    error ? theme.colors.danger : theme.colors.heroItemBackground
  };
  padding: 10px;
  font-size: 18px;
  color: ${theme.colors.textBlack};
  width: 100%;

  :focus {
    outline: none;
  }
`;

export const Input = styled.input`
  display: ${({ disabled }) => (disabled ? 'none' : 'visible')};
  ${inputStyling}
`;

export const Textarea = styled.textarea`
  ${inputStyling}
  resize: none;
`;

export const InputGroup = ({ label, children }) => {
  const id = label.toLowerCase().replace(' ', '');

  return (
    <InputGroupWrapper>
      <Label htmlFor={id}>{label}</Label>
      {children}
    </InputGroupWrapper>
  );
};

InputGroup.propTypes = {
  label: pt.string.isRequired,
  children: pt.node.isRequired,
};

export const Select = styled.select`
  appearance: none;
  ${inputStyling}
`;
