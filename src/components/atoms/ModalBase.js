import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import pt from 'prop-types';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

const ModalBackdrop = styled.div.attrs(({ onClick }) => ({ onClick }))`
  position: fixed;
  z-index: 1;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: black;
  opacity: 0.6;
`;

const ModalBody = styled.div.attrs(({ ref }) => ({ ref }))`
  background-color: ${({ theme }) => theme.colors.rootBackground};
  position: fixed;
  z-index: 2;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  max-width: 425px;
  max-height: 100vh;
  overflow-y: auto;

  padding: 15px;
  border-radius: 10px;

  @media (max-width: 425px) {
    height: 100vh;
  }
`;

export const ModalContent = styled.div`
  display: flex;
  flex-flow: column nowrap;
  height: 100%;
`;

const CloseIcon = styled.i.attrs(({ onClick }) => ({
  className: 'icon-cancel',
  onClick,
}))`
  position: absolute;
  right: 25px;
  top: 35px;
  font-size: 24px;
  color: ${({ theme }) => theme.colors.textDark};
  cursor: pointer;
`;

export const Modal = ({ children, onClose }) => {
  const modal = useRef(null);

  useEffect(() => {
    const handleEscPress = ({ key }) => {
      if (key === 'Escape') {
        onClose();
      }
    };
    document.addEventListener('keyup', handleEscPress);

    return () => document.removeEventListener('keyup', handleEscPress);
  }, [onClose]);

  useEffect(() => {
    const modalRef = modal.current;

    if (modalRef) {
      disableBodyScroll(modalRef);
    }

    return () => enableBodyScroll(modalRef);
  }, [modal]);

  return (
    <>
      <ModalBackdrop onClick={onClose} />
      <ModalBody ref={modal}>
        <CloseIcon onClick={onClose} />
        {children}
      </ModalBody>
    </>
  );
};

Modal.propTypes = {
  children: pt.node.isRequired,
  onClose: pt.func.isRequired,
};
