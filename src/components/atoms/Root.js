import styled from 'styled-components';

const Root = styled.div`
  width: 100%;
  min-height: 100vh;

  background-color: ${({ theme }) => theme.colors.rootBackground};
`;

export default Root;
