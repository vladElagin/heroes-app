import styled from 'styled-components';

const Avatar = styled.img.attrs((props) => ({
  src: props.src,
  alt: '',
}))`
  border-radius: 50%;
  width: 100%;
  object-fit: cover;
`;

export default Avatar;
