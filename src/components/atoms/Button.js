import React from 'react';
import styled from 'styled-components';
import pt from 'prop-types';

const StyledButton = styled.button`
  padding: 0 15px;
  height: 45px;
  line-height: 45px;
  border-radius: 10px;
  font-size: 16px;

  border: none;
`;

export const Button = ({ children, className, onClick, type }) => (
  <StyledButton className={className} onClick={onClick} type={type}>
    {children}
  </StyledButton>
);

Button.propTypes = {
  children: pt.node.isRequired,
  className: pt.string.isRequired,
  onClick: pt.func,
  type: pt.string,
};

Button.defaultProps = {
  onClick: () => null,
  type: 'button',
};

export const SuccessButton = styled(Button)`
  color: white;
  background-color: ${({ theme }) => theme.colors.success};

  opacity: ${({ disabled }) => (disabled ? '0.4' : '1')};
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
`;

export const PrimaryButton = styled(Button)`
  color: white;
  background-color: ${({ theme }) => theme.colors.primary};
`;

export const DangerButton = styled(Button)``;
