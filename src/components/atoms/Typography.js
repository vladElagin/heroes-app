import styled from 'styled-components';

/**
 * Small (1-2 sentences) text with large font that notifies user about something.
 */
export const Notification = styled.p`
  margin: 0;
  font-size: ${({ theme }) => theme.fontSizes.lg};
  font-weight: ${({ theme }) => theme.fontWeights.semibold};
  color: ${({ theme }) => theme.colors.textBlack};
`;

export const Text = styled.span``;

export const EmphasisedText = styled.span`
  font-weight: ${({ theme }) => theme.fontWeights.semibold};
  font-size: larger;
  color: ${({ theme }) => theme.colors.textBlack};
`;

export const TruncatedText = styled.span`
  width: calc(100%);
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;
