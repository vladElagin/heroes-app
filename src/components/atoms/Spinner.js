import React from 'react';
import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const SpinnerIcon = styled.i.attrs({
  className: 'icon-spinner',
  'data-testid': 'spinner',
})`
  font-size: 45px;
  color: ${({ theme }) => theme.colors.success};
  animation: ${rotate} 1.3s linear infinite;
`;

const SpinnerBackdrop = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  display: flex;
  justify-content: center;
  align-items: center;

  background-color: white;
  opacity: 0.7;
`;

const Spinner = () => (
  <SpinnerBackdrop>
    <SpinnerIcon />
  </SpinnerBackdrop>
);

export default Spinner;
